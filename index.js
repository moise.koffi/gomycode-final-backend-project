import express from 'express';
import mongoose from 'mongoose';
import cors from 'cors';

const app = express();
const PORT = process.env.PORT || 5000;

app.use(express.json());
app.use(cors());

mongoose.connect('mongodb://localhost:27017/studentNotes')
    .then(() => console.log('Connexion à MongoDB réussie'))
    .catch((err) => console.error('Erreur de connexion à MongoDB:', err));

const studentSchema = new mongoose.Schema({
    firstName: String,
    lastName: String,
    studentNumber: String,
    notes: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Note' }] // Ajout du champ notes pour stocker les ID des notes associées à chaque étudiant
});

const Student = mongoose.model('Student', studentSchema);

const noteSchema = new mongoose.Schema({
    student: { type: mongoose.Schema.Types.ObjectId, ref: 'Student' },
    subject: String,
    score: Number
});

const Note = mongoose.model('Note', noteSchema);

// Ajouter un nouvel étudiant
app.post('/students', async (req, res) => {
    try {
        const { firstName, lastName, studentNumber } = req.body;
        const student = new Student({ firstName, lastName, studentNumber });
        await student.save();
        res.status(201).json({ success: true, message: 'Étudiant ajouté avec succès', data: student });
    } catch (error) {
        res.status(500).json({ success: false, message: 'Erreur lors de l\'ajout de l\'étudiant', error: error.message });
    }
});

// Récupérer tous les étudiants avec leurs notes
app.get('/students', async (req, res) => {
    try {
        const students = await Student.find().populate('notes');
        res.status(200).json({ success: true, data: students });
    } catch (error) {
        res.status(500).json({ success: false, message: 'Erreur lors de la récupération des étudiants', error: error.message });
    }
});

// Récupérer un étudiant par son ID
app.get('/students/:id', async (req, res) => {
    try {
        const student = await Student.findById(req.params.id).populate('notes');
        if (!student) {
            return res.status(404).json({ success: false, message: 'Étudiant non trouvé' });
        }
        res.status(200).json({ success: true, data: student });
    } catch (error) {
        res.status(500).json({ success: false, message: 'Erreur lors de la récupération de l\'étudiant', error: error.message });
    }
});

// Mettre à jour un étudiant
app.put('/students/:id', async (req, res) => {
    try {
        const { id } = req.params;
        const { firstName, lastName, studentNumber } = req.body;
        const updatedStudent = await Student.findByIdAndUpdate(id, { firstName, lastName, studentNumber }, { new: true });
        res.status(200).json({ success: true, message: 'Étudiant mis à jour avec succès', data: updatedStudent });
    } catch (error) {
        res.status(500).json({ success: false, message: 'Erreur lors de la mise à jour de l\'étudiant', error: error.message });
    }
});

// Supprimer un étudiant
app.delete('/students/:id', async (req, res) => {
    try {
        const { id } = req.params;
        await Student.findByIdAndDelete(id);
        res.status(200).json({ success: true, message: 'Étudiant supprimé avec succès' });
    } catch (error) {
        res.status(500).json({ success: false, message: 'Erreur lors de la suppression de l\'étudiant', error: error.message });
    }
});

// Récupérer toutes les notes
app.get('/notes', async (req, res) => {
    try {
        const notes = await Note.find();
        res.status(200).json({ success: true, data: notes });
    } catch (error) {
        res.status(500).json({ success: false, message: 'Erreur lors de la récupération des notes', error: error.message });
    }
});

// Récupérer les notes d'un étudiant par son ID
app.get('/notes/:studentId', async (req, res) => {
    try {
        const { studentId } = req.params;
        const notes = await Note.find({ student: studentId });
        res.status(200).json({ success: true, data: notes });
    } catch (error) {
        res.status(500).json({ success: false, message: 'Erreur lors de la récupération des notes de l\'étudiant', error: error.message });
    }
});

// Mettre à jour une note
app.put('/notes/:id', async (req, res) => {
    try {
        const { id } = req.params;
        const { subject, score } = req.body;
        const updatedNote = await Note.findByIdAndUpdate(id, { subject, score }, { new: true });
        res.status(200).json({ success: true, message: 'Note mise à jour avec succès', data: updatedNote });
    } catch (error) {
        res.status(500).json({ success: false, message: 'Erreur lors de la mise à jour de la note', error: error.message });
    }
});

// Supprimer une note
app.delete('/notes/:id', async (req, res) => {
    try {
        const { id } = req.params;
        await Note.findByIdAndDelete(id);
        res.status(200).json({ success: true, message: 'Note supprimée avec succès' });
    } catch (error) {
        res.status(500).json({ success: false, message: 'Erreur lors de la suppression de la note', error: error.message });
    }
});

// Ajouter une note à un étudiant existant
app.post('/notes', async (req, res) => {
    try {
        const { studentId, subject, score } = req.body;
        const note = new Note({ student: studentId, subject, score });
        await note.save();
        // Mettre à jour les notes associées à l'étudiant
        await Student.findByIdAndUpdate(studentId, { $push: { notes: note._id } });
        res.status(201).json({ success: true, message: 'Note ajoutée avec succès', data: note });
    } catch (error) {
        res.status(500).json({ success: false, message: 'Erreur lors de l\'ajout de la note', error: error.message });
    }
});

app.listen(PORT, () => {
    console.log(`Serveur en cours d'exécution sur le port ${PORT}`);
});
